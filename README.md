# GladePay VirtueMart3 Payment Plugin

Payment Plugin for VirtueMart3

# GladePay VirtueMart Payment Plugin

This is a payment plugin for virtuemart3.
It is built for integration with virtuemart3 stores set up on JOOMLA Content Management System (CMS).

#Prerequisite
-Shop owner has signed up with GladePay as a Merchant at: https://dashboard.gladepay.com/register

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
1. Download and install JOOMLA CMS on your system

2. Download and install VirtueMart3 as a plugin extension through the JOOMLA Admin Dashboard (https://virtuemart.net/downloads).	
	Login to JOOMLA admin dashboard->Extensions->Manage->Install. Drag and drop the zipped installation folders 
	There are three virtuemart3 mandatory files that must be installed in the following sequence:
	a. com_virtuemart.3.x.x.xxx.zip  #virtuemart store installer
	
	b. com_virtuemart.3.x.x.xxx_ext_aio.zip #this installs all modules and required virtuemart3 plugins for your store
		-Setup sample store data for your store after 'b' and reset database values.
		
	c. com_tcpdf_1.x.x.zip #free Open Source PHP class for generating PDF documents
	

3. Download the VirtueMart plugin zipped folder from https://developer.gladepay.com/sdk-plugins/
4. Install the zipped folder following the same steps as in step '2' above.
5. Enable Gladepay as a payment gateway by:
	a. Go to VirueMart Navigation Link
	b. Select 'Payment Methods' from the drop down
	c. Click 'Add New' Payment or the green '+' symbol to configure a new payment modules
	d. Name the payment method as 'GladePay'
	e. Name the alias as 'Make Payment'
	f. Set 'Published' option to 'YES'
	g. Select 'VM Payment - GladePay' as the payment method.
	h. Set 'Shopper Group' to 'Available for all'
	i. Set 'Currency' to 'Nigerian naira'
	j. Save and Close.

#Configuration settings
6. Select the newly created payment method under the 'Payment Methods' under VirtueMart
	a. Go to Configuration (just beside the 'Payment Method Information')
	b. Select Test Mode: YES (NB: You need to change this to 'NO' when going live)
	c. Use Merchant ID: GP0000001 and Merchant Key: 123456789 for test account settings. (NB: Use the Merchant Key and Merchant ID from your GladePay merchant dashboard as your 'live' credentials)
	d. Save and Close.
	
#Enable VirtueMart3
7. Go to Joomla admin dashboard and enable virtuemart


#Testing
To run tests:
1. Set up your local server
2. Navigate to your Joomla folder through your browser and access your store.
3. Run tests by carrying out your normal shopping activities.
4. During checkout, select 'GladePay' as your payment method, agree to VirtueMart terms of service and Confirm payment.
5. Click 'Pay with GladePay' and get redirected to our simple popup inline checkout modal.
6. Make use of one of the available payment options (Card, Bank Account, USSD, QR Code Scan and Mobile money) and make payment.
7. Invoice and Printable receipt is generated when customer gets redirected to VirtueMart

## Deployment
-Change configuration settings for the plugin on Joomla admin dashboard:
Navigate to: VirtueMart->Payment Methods->GladePay->Configuration.
Set Test Mode to 'NO', provide Live Merchant Key and Merchant ID to the values gotten from Gladepay's merchant dashboard: https://dashboard.gladepay.com
-Save and Close

## Built With
-VirtueMart3 payment plugin development tools
-Configured on Joomla Admin Dashboard
-Deployed on existing VirtueMart Store through Joomla admin dashboard

## Contributing

## Version
1.0

## Authors
GladePay.
Contributor and Support (email): a.anthony@gladepay.com

## License

This project is licensed under the MIT License 


#How to install
![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
![](images/06.png)
![](images/07.png)
![](images/08.png)
![](images/09.png)
![](images/11.png)
![](images/23.png)
![](images/24.png)
![](images/12.png)
![](images/13.png)
![](images/14.png)
![](images/16.png)
![](images/17.png)
![](images/18.png)
![](images/19.png)
![](images/20.png)
![](images/21.png)
![](images/22.png)



## Acknowledgments
*Joomla Developer Support Forum
*Existing virtuemart payment plugin developers and payment gateway companies
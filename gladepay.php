<?php

/**
 * @package       VM Payment - GladePay VM3 Plugin
 * @author        Anthony: a.anthony@gladepay.com
 * @copyright     Copyright (C) 2018 Gladepay Ltd. All rights reserved.
 * @version       1.0 December 2018
 * @license       GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die('Direct access to ' . basename(__FILE__) . ' is not allowed.');

if (!class_exists('vmPSPlugin'))
    require(JPATH_VM_PLUGINS . DIRECTORY_SEPARATOR . 'vmpsplugin.php');

class plgVmPaymentGladepay extends vmPSPlugin
{
    function __construct(&$subject, $config)
    {
        parent::__construct($subject, $config);

        $this->_loggable  = true;
        $this->_tablepkey = 'id';
        $this->_tableId   = 'id';

        $this->tableFields = array_keys($this->getTableSQLFields());

        
        //GLADEPAY FIELDS
        $varsToPush = array(
            'test_mode' => array(
                1,
                'int'
            ), // gladepay.xml (test_mode)
            'live_merchant_key' => array(
                '',
                'char'
            ), // gladepay.xml (live_merchant_key)
            'live_merchant_id' => array(
                '',
                'char'
            ), // gladepay.xml (live_merchant_id)
            'test_merchant_key' => array(
                '',
                'char'
            ), // gladepay.xml (test_merchant_key)
            'test_merchant_id' => array(
                '',
                'char'
            ), // gladepay.xml (test_merchant_id)

            //virtuemart transaction statuses
            'status_pending' => array(
                '',
                'char'
            ),
            'status_success' => array(
                '',
                'char'
            ),
            'status_canceled' => array(
                '',
                'char'
            ),

            //virtuemart cart amount config
            'min_amount' => array(
                0,
                'int'
            ),
            'max_amount' => array(
                0,
                'int'
            ),
            'cost_per_transaction' => array(
                0,
                'int'
            ),
            'cost_percent_total' => array(
                0,
                'int'
            ),
            'tax_id' => array(
                0,
                'int'
            )
        );

        //config passed to class method
        $this->setConfigParameterable($this->_configTableFieldName, $varsToPush);
    }

    public function getVmPluginCreateTableSQL()
    {
        // create PAYMENT GLADEPAY TABLE.
        return $this->createTableSQL('Payment Gladepay Table');
    }


    //create table columns
    function getTableSQLFields()
    {
        $SQLfields = array(
            'id' => 'tinyint(1) unsigned NOT NULL AUTO_INCREMENT',
            'virtuemart_order_id' => 'int(11) UNSIGNED DEFAULT NULL',
            'order_number' => 'char(32) DEFAULT NULL',
            'virtuemart_paymentmethod_id' => 'mediumint(1) UNSIGNED DEFAULT NULL',
            'payment_name' => 'char(255) NOT NULL DEFAULT \'\' ',
            'payment_order_total' => 'decimal(15,5) NOT NULL DEFAULT \'0.00000\' ',
            'payment_currency' => 'char(3) ',
            'cost_per_transaction' => ' decimal(10,2) DEFAULT NULL ',
            'cost_percent_total' => ' decimal(10,2) DEFAULT NULL ',
            'tax_id' => 'smallint(11) DEFAULT NULL',
            'gladepay_transaction_reference' => 'char(32) DEFAULT NULL'
        );

        return $SQLfields;
    }

    //Get Gladepay configuration settings
    function getGladepaySettings($payment_method_id)
    {
        //load Virtuemart configuration settings into variable
        $gladepay_settings = $this->getPluginMethod($payment_method_id);

        //If configuration says 'test mode', use these config keys and endpoints
        if ($gladepay_settings->test_mode) {
    
            //test config and keys
            $endpoint = "https://demo.api.gladepay.com/payment"; //live endpoint
            $inline_js = "http://demo.api.gladepay.com/checkout.js"; //live js endpoint
            $merchant_key = $gladepay_settings->test_merchant_key;
            $merchant_id = $gladepay_settings->test_merchant_id;
        } else {
            
            //live config and keys
            $endpoint = "https://api.gladepay.com/payment"; //live endpoint
            $inline_js = "http://api.gladepay.com/checkout.js"; //live js endpoint
            $merchant_key = $gladepay_settings->live_merchant_key;
            $merchant_id = $gladepay_settings->live_merchant_id;
        }

        //returned configuration values based on selected mode (test or live) on virtuemart admin dashboard
        return array( 
            'key' => $merchant_key,      
            'mid' => $merchant_id,
            'endpoint' => $endpoint, 
            'inline_js' => $inline_js,
        );
    }

    
    //This method sends to- and receives responses from - gladepay INLINE checkout endpoint
    function verifyGladepayTransaction($txnRef, $payment_method_id)
    {
        $transactionStatus        = new stdClass();
        $transactionStatus->error = "";

        // Get Gladepay config based on selected virtuemart mode (live or test mode).
        //This variable contains all values returned from getGladepaySettings() method as an associative array
        $gladepay_settings = $this->getGladepaySettings($payment_method_id);

            //This array is contains the required information needed to verify transactions from gladepay endpoint
            //It is later encoded to JSON format being before sent to gladepay INLINE CHECKOUT endpoint
            $payload = [
                "action" => "verify",
                'txnRef' => $txnRef
            ];

            //GLADEPAY VERIFICATION request sent via CURL. (this is for php)
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => $gladepay_settings['endpoint'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => json_encode($payload),
            CURLOPT_HTTPHEADER => array(
                "key: ".$gladepay_settings['key'],
                "mid: ".$gladepay_settings['mid']
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
            return "cURL Error #:" . $err;
            } else {
            
            //string response received from gladepay
            return $response;

            }

        //Are there any problems in txn status? Checked here
        if ($response) {
            $body = json_decode($response); //response is converted to associative array
            if (!$body->status) {

                //GLADEPAY API SAYS An error occured
                $transactionStatus->error = "Gladepay could not process this transaction. Contact the store merchant";
            } else {
                // get transaction status returned by Gladeoay API
                $transactionStatus = $body->txnStatus;
            }
        } else {
            // no response
            $transactionStatus->error = $transactionStatus->error . " : No response";
        }
        //THIS IS THE TRANSACTION STATUS AS RETURNED FROM GLADEPAY. It's string returned
        return $transactionStatus;
    }  

    //THIS Sends checkout data from Virtuemart to Gladepay. Don't bother editing
    function plgVmConfirmedOrder($cart, $order)
    {
        if (!($method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
            return null;
        }

        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }

        if (!class_exists('VirtueMartModelOrders'))
            require(JPATH_VM_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'orders.php');

        if (!class_exists('VirtueMartModelCurrency'))
            require(JPATH_VM_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'currency.php');

        // Get current order info from virtuemart cart page
        $order_info   = $order['details']['BT'];

        $country_code = ShopFunctions::getCountryByID($order_info->virtuemart_country_id, 'country_3_code');

        // Get payment currency
        $this->getPaymentCurrency($method);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
                    ->select($db->quoteName('currency_code_3'))
                    ->from($db->quoteName('#__virtuemart_currencies'))
                    ->where($db->quoteName('virtuemart_currency_id')
                      . ' = '. $db->quote('\''.$method->payment_currency.'\''));
        $db->setQuery($query);
        $currency_code = $db->loadResult();


        //get payment currency short code (e.g NGN, EUR, etc...)
        //We accept Dollar and Nigerian Naira only. You may change your default currency in joomla admin panel
        if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'config.php');
        $config = VmConfig::loadConfig();
        $currency_model = VmModel::getModel('currency');
        $displayCurrency = $currency_model->getCurrency( $this->product->product_currency );
        $displayCurrency->currency_code_3; //This is the order currency short code
        

        // Get total amount for the current payment currency
        $totalInPaymentCurrency = vmPSPlugin::getAmountInCurrency($order['details']['BT']->order_total, $method->payment_currency);

        // Prepare data that should be stored in the database
        $dbValues['order_number']                   = $order['details']['BT']->order_number;
        $dbValues['payment_name']                   = $this->renderPluginName($method, $order);
        $dbValues['virtuemart_paymentmethod_id']    = $order['details']['BT']->virtuemart_paymentmethod_id;
        $dbValues['cost_per_transaction']           = $method->cost_per_transaction;
        $dbValues['cost_percent_total']             = $method->cost_percent_total;
        $dbValues['payment_currency']               = $method->payment_currency;
        $dbValues['payment_order_total']            = $totalInPaymentCurrency;
        $dbValues['tax_id']                         = $method->tax_id;
        $dbValues['gladepay_transaction_reference'] = $dbValues['order_number'] . '-' . date('YmdHis');

        $this->storePSPluginInternalData($dbValues);


        // Return URL - After verifying Gladepay payment, response sent with this url
        $return_url = JURI::root() . 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginresponsereceived&on=' . $order['details']['BT']->order_number . '&pm=' . $order['details']['BT']->virtuemart_paymentmethod_id . '&Itemid=' . vRequest::getInt('Itemid') . '&lang=' . vRequest::getCmd('lang', '');


       
        // Gladepay Settings
        $payment_method_id = $dbValues['virtuemart_paymentmethod_id'];
        $gladepay_settings = $this->getGladepaySettings($payment_method_id);

        //get merchant id from set configurations.
        //Ensure to get live merchant id and key from your gladepay dashboard
        $mid = $this->getGladepaySettings($payment_method_id)['mid'];

        //get customer details from Virtue Mart Checkout billing details
        $customer_email   = $order['details']['BT']->email;
        $customer_firstname   = $order['details']['BT']->first_name;
        $customer_lastname   = $order['details']['BT']->last_name;
        $customer_title   = $order['details']['BT']->title;
        $order_number = $dbValues['order_number']; //This order number becomes the description details sent to the inline checkout
        $order_currency = $displayCurrency->currency_code_3; //INSERT into html after testing

        //get inline checkout Javascript src link
        $inline_js_src = $this->getGladepaySettings($payment_method_id)['inline_js'];

        //Gladepay checkout interface
        //checkout information sent to inline checkout interface here
        $html = '
        <!DOCTYPE html>
        <html>
            <body onload="pay()">
                <script type="text/javascript" src="'.$inline_js_src.'"></script>
                <script>

                    function pay(){
                        initPayment({
                            MID:"'.$mid.'",
                            email: "'.$customer_email.'",
                            firstname:"'.$customer_firstname.'",
                            lastname:"'.$customer_lastname.'",
                            description: "Order Number: '.$order_number.'",
                            title: "'.$customer_title.'",
                            amount: "'.$dbValues['payment_order_total']['value'].'",
                            country: "'.$country_code.'",
                            currency: "'.$order_currency.'",
                            onclose: function() {
                            },
                            callback: function(response) {
                            var gladepay_response = JSON.stringify(response);    
                            window.location.href = "'.$return_url.'&response="+gladepay_response;              
                            }
                            
                        });
                    }

                </script>
        </body>
    </html>
    ';


        $cart->_confirmDone   = FALSE;
        $cart->_dataValidated = FALSE;
        $cart->setCartIntoSession();        

          //THIS SETS VM3 HTML UP, loads gladepay interface unto virtuemart
        vRequest::setVar('html', $html);
    }

    //METHOD FOR RESPONSE. VIRTUEMART SPECIFIC AND NEEDS NO EDITING
     function plgVmOnPaymentResponseReceived(&$html)
    {
        if (!class_exists('VirtueMartCart')) {
            require(VMPATH_SITE . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'cart.php');
        }
        if (!class_exists('shopFunctionsF')) {
            require(VMPATH_SITE . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'shopfunctionsf.php');
        }
        if (!class_exists('VirtueMartModelOrders')) {
            require(VMPATH_ADMIN . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'orders.php');
        }

        VmConfig::loadJLang('com_virtuemart_orders', TRUE);
        $post_data = vRequest::getPost();

        // The payment itself should send the parameter needed.
        $virtuemart_paymentmethod_id = vRequest::getInt('pm', 0);
        $order_number                = vRequest::getString('on', 0);
        if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
            return NULL;
        }

        if (!$this->selectedThisElement($method->payment_element)) {
            return NULL;
        }

        if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number))) {
            return NULL;
        }

        if (!($paymentTable = $this->getDataByOrderId($virtuemart_order_id))) {
            return '';
        }

        VmConfig::loadJLang('com_virtuemart');
        $orderModel = VmModel::getModel('orders');
        $order      = $orderModel->getOrder($virtuemart_order_id);

        $payment_name = $this->renderPluginName($method);
        $html = '<table>' . "\n";
        $html .= $this->getHtmlRow('Payment Name', $payment_name);
        $html .= $this->getHtmlRow('Order Number', $order_number);


        //get full response from gladepay
        $gladepayTxnRef = json_decode($_GET['response'], true);
        
        //GET transaction reference from gladepay for verification purposes
        $txnRef = $gladepayTxnRef['txnRef'];
        

        //verifyGladepayTransaction Returns a string which is the response from the inline checkout
        $transData = $this->verifyGladepayTransaction($txnRef, $virtuemart_paymentmethod_id);

        //convert string to an associative array to be accessed by objects
        $transData = json_decode($transData);

        //Confirm there is NO ERROR, txn status is successful and fraudstatus is ok from Gladepay.
        if (!property_exists($transData, 'error') && property_exists($transData, 'txnStatus') && ($transData->txnStatus === 'successful') && ($transData->fraudStatus==='ok')) {
            
            // Update order status - From pending to complete
            $order['order_status']      = 'C'; //confirmed status
            $order['customer_notified'] = 1;
            $orderModel->updateStatusForOneOrder($order['details']['BT']->virtuemart_order_id, $order, TRUE);

            $html .= $this->getHtmlRow('Charged Amount', $order_currency.$transData->chargedAmount, 2);
            $html .= $this->getHtmlRow('Status', $transData->txnStatus);
            $html .= '</table>' . "\n";
            // add order url. 
            $url=JRoute::_('index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$order_number,FALSE);
            
            //Prints invoice
            $html.='<a href="'.JRoute::_('index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$order_number,FALSE).'" class="vm-button-correct">'.vmText::_('COM_VIRTUEMART_ORDER_VIEW_ORDER').'</a>';

            // Empty cart
            $cart = VirtueMartCart::getCart();
            $cart->emptyCart();

            return True;
        } else if (property_exists($transData, 'error')) {
            die($transData->error);
        } else {
            $html .= $this->getHtmlRow('Charged amount', $transData->chargedAmount, 2);
            $html .= $this->getHtmlRow('Status', $transData->txnStatus);
            $html .= '</table>' . "\n";
            $html.='<a href="'.JRoute::_('index.php?option=com_virtuemart&view=cart',false).'" class="vm-button-correct">'.vmText::_('CART_PAGE').'</a>';

            // Update order status - From pending to cancelled
            $order['order_status']      = 'X';
            $order['customer_notified'] = 1;
            $orderModel->updateStatusForOneOrder($order['details']['BT']->virtuemart_order_id, $order, TRUE);
        }

        return False;
    }

    //LEAVE AS IT IS. VM3 method triggered when order is cancelled
    function plgVmOnUserPaymentCancel()
    {
        return true;
    }

    /**
     * Required functions by Joomla or VirtueMart. Removed code comments due to 'file length'.
     * All copyrights are (c) respective year of author or copyright holder, and/or the author.
     */
    //ALL RIGHTS RESERVED FOR ALL ORIGINATING AUTHORS WHO HAVE SEPARATED THIS NEATLY. Leave untouched.
    function getCosts(VirtueMartCart $cart, $method, $cart_prices)
    {
        if (preg_match('/%$/', $method->cost_percent_total)) {
            $cost_percent_total = substr($method->cost_percent_total, 0, -1);
        } else {
            $cost_percent_total = $method->cost_percent_total;
        }
        return ($method->cost_per_transaction + ($cart_prices['salesPrice'] * $cost_percent_total * 0.01));
    }

    protected function checkConditions($cart, $method, $cart_prices)
    {
        $this->convert_condition_amount($method);
        $address     = (($cart->ST == 0) ? $cart->BT : $cart->ST);
        $amount      = $this->getCartAmount($cart_prices);
        $amount_cond = ($amount >= $method->min_amount AND $amount <= $method->max_amount OR ($method->min_amount <= $amount AND ($method->max_amount == 0)));
        $countries   = array();
        if (!empty($method->countries)) {
            if (!is_array($method->countries)) {
                $countries[0] = $method->countries;
            } else {
                $countries = $method->countries;
            }
        }
        if (!is_array($address)) {
            $address                          = array();
            $address['virtuemart_country_id'] = 0;
        }
        if (!isset($address['virtuemart_country_id'])) {
            $address['virtuemart_country_id'] = 0;
        }
        if (in_array($address['virtuemart_country_id'], $countries) || count($countries) == 0) {
            if ($amount_cond) {
                return TRUE;
            }
        }
        return FALSE;
    }

    function plgVmOnStoreInstallPaymentPluginTable($jplugin_id)
    {
        return $this->onStoreInstallPluginTable($jplugin_id);
    }

    public function plgVmOnSelectCheckPayment(VirtueMartCart $cart)
    {
        return $this->OnSelectCheck($cart);
    }

    public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn)
    {
        return $this->displayListFE($cart, $selected, $htmlIn);
    }

    public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name)
    {
        return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
    }

    function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array(), &$paymentCounter)
    {
        return $this->onCheckAutomaticSelected($cart, $cart_prices, $paymentCounter);
    }

    public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name)
    {
        $this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
    }

    function plgVmonShowOrderPrintPayment($order_number, $method_id)
    {
        return $this->onShowOrderPrint($order_number, $method_id);
    }

    function plgVmDeclarePluginParamsPaymentVM3(&$data)
    {
        return $this->declarePluginParams('payment', $data);
    }

    function plgVmSetOnTablePluginParamsPayment($name, $id, &$table)
    {
        return $this->setOnTablePluginParams($name, $id, $table);
    }

}
